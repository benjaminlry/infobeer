import React, { Component } from "react"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {getBeers, getBeer} from "../actions/index.js"

class DropList extends Component {
    constructor(props) {
        super(props);
        this.state = {selectedBeer:""};
    }

    componentWillMount() {
        this.props.getBeers();
        this.props.getBeer(1);
    }

    renderSelectBeers() {
        const {beers} = this.props
        if(beers) {
            return (
                <select value={this.state.selectedBeer} onChange={(e) => this.search(e)} className="col-lg-5 input-group">
                    {
                        beers.map((beer) => {
                            var optionEbc = ``;
                            var optionColor = ``;
                            var ourClasses = ``;
                            
                            optionEbc = this.getClassColorBeer(beer.ebc)
                            optionColor = this.colorText(beer.ebc)
                            ourClasses = optionEbc + " "+ optionColor

                            return <option key={beer.id} data-id={beer.id} value={beer.name} className={ourClasses}>{beer.name}</option>
                        })
                    }
                </select>
            )
        } else {
            return <div>Aucune bière trouvé !</div>
        }
    }

    renderSelectedBeer() {
        const {beer} = this.props
        if(beer) {
            var divEbc = `ebcDiv`
            var divColorEbc = ``
            var ebc = ''

            ebc = this.getClassColorBeer(beer[0].ebc)
            divEbc = `ebcDiv `+ebc
            divColorEbc = this.colorText(beer[0].ebc)

            if(document.querySelector(".container").classList[1]) {
                var el = document.querySelector(".container").classList[1];
                document.querySelector(".container").classList.remove(el)
            }
            document.querySelector(".container").classList.add(ebc)

            var styleDivInfo = "col-lg-6 infoBeer "+divColorEbc

            var amertumePercent = beer[0].ibu * 100 / 150;
            var amertumePercentArround = Math.round(amertumePercent);
            var styleAmertume = {
                width: amertumePercent+'%'
            }
            var classesAmertume = "divAmertume "+ebc

            var degrePercent = beer[0].abv;
            var styleDegre = {
                width: degrePercent+'%'
            }
            var classesDegre = "divDegre "+ebc

            return (
                <div className={styleDivInfo}>
                    <div className="col-lg-3">
                        <img src={beer[0].image_url} className="image_beer"/>
                    </div>
                    <div className="col-lg-9">
                        <h2>{beer[0].name}</h2>
                        <h5 className="slogan">{beer[0].tagline}</h5>
                        <p>{beer[0].description}</p>
                        <p>Première brassé : {beer[0].first_brewed}</p>
                        <p className="margin-bottom-0">Degré ({degrePercent}%)</p>
                        <div className="containerDegre">
                            <div className="divBackgroundDegre"></div>
                            <div className={classesDegre} style={styleDegre}></div>
                        </div>
                        <p className="margin-bottom-0">Amertume ({amertumePercentArround}%)</p>
                        <div className="containerAmertume">
                            <div className="divBackgroundAmertume"></div>
                            <div className={classesAmertume} style={styleAmertume}></div>
                        </div>
                        <p>Volume : {beer[0].volume.value} {beer[0].volume.unit}</p>
                    </div>
                </div>
            )
        }
    }

    // Cette fonction permet de renvoyer une classe si le paramètre saisie est supérieur à 30 (ce qui signifie une couleur sombre)
    // Param : value = beer.ebc
    colorText(value) {
        if(value >= 30 || value == 0) {
            return `color-white`
        } else {
            return `color-black`
        }
    }

    // Cette fonction retourne une classe correspondant à une couleur
    // Param : value = beer.ebc
    getClassColorBeer(value) {
        if(value > 60) {
            return `ebc-60`
        } else if(value != null) {
            return `ebc-${value}`
        } else {
            return `ebc-0`
        }
    }

    search(e) {
        this.setState({selectedBeer:e.target.value});
        var beerId = e.target.childNodes[e.target.selectedIndex].getAttribute('data-id');
        console.log("Beer id selected : "+beerId)
        this.props.getBeer(beerId);
    }

    render() {
        return (
            <div className="search_bar">
                {this.renderSelectBeers()}
                {this.renderSelectedBeer()}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        beers: state.beers,
        beer: state.beer
    }
}

function mapDispatchToProps(dispatch) {
    //return bindActionCreators(Object.assign({}, getBeers, getBeer), dispatch)
    return {
        getBeers: bindActionCreators(getBeers, dispatch),
        getBeer: bindActionCreators(getBeer, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DropList)