import React, { Component } from 'react';
import DropList from "../containers/droplist";

export default class App extends Component {
  render() {
    return (
      <div>
        <h1 className="mainTitle">InfoBeer</h1>
        <DropList/>
      </div>
    );
  }
}
