import axios from 'axios';

export const GET_BEERS = "GET_BEERS";
export const ERROR_GET_BEERS = "ERROR_GET_BEERS";

export const GET_BEERS_ID = "GET_BEERS_ID";
export const ERROR_GET_BEERS_ID = "ERROR_GET_BEERS_ID";

export function getBeers() {
    console.log("getBeers")
    return function(dispatch) {
        axios("https://api.punkapi.com/v2/beers").then(function(response) {
            dispatch({type:GET_BEERS,payload:response.data})
        }).catch(function(error) {
            dispatch({type:ERROR_GET_BEERS,error:error.response.data.detail})
        })
    }
}

export function getBeer(id) {
    return function(dispatch) {
        axios(`https://api.punkapi.com/v2/beers/${id}`).then(function(response) {
            dispatch({type:GET_BEERS_ID,payload:response.data})
        }).catch(function(error) {
            dispatch({type:ERROR_GET_BEERS_ID,error:error.response.data.detail})
        })
    }
}