import {GET_BEERS_ID, ERROR_GET_BEERS_ID} from "../actions/index.js"

export default function (state=null, action) {
    switch(action.type) {
        case GET_BEERS_ID:
            return action.payload
        case ERROR_GET_BEERS_ID:
            return action.error
    }
    return state
}