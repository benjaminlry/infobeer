import { combineReducers } from 'redux';
import BeersReducer from './reducer-beers'
import BeerReducer from './reducer-beer'

const rootReducer = combineReducers({
  beers: BeersReducer,
  beer: BeerReducer
});

export default rootReducer;
