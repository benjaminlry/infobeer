import {GET_BEERS, ERROR_GET_BEERS} from "../actions/index.js"

export default function (state=null, action) {
    switch(action.type) {
        case GET_BEERS:
            return action.payload
        case ERROR_GET_BEERS:
            return action.error
    }
    return state
}