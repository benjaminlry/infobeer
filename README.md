### InfoBeer ###
Ce projet est connecté à l'API PunkAPI (https://punkapi.com/documentation/v2).


Sur cette application vous avez une liste des bières, après en avoir sélectionné une bière vous avez sa couleur en arrière plan, son nom, sa description et divers informations comme son degré, son amertume ou encore sa date de première brassée.

Pour installer le projet se placer dans le projet et lancer : 

```
> npm install
> npm start
```

Avec la mission d'entreprise (en partie le mémoire) et le projet collectif, je n'ai pas eu le temps de passer beaucoup de temps, comme vous l'avez dit en cours 2h suffisent donc voilà mon travail de 2h.
Bien à vous

